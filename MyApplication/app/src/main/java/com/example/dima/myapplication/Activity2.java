package com.example.dima.myapplication;

import android.content.Intent;
import android.preference.EditTextPreference;
import android.sax.StartElementListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(getBaseContext().getString(R.string.Hello) + " " + getIntent().getStringExtra(MainActivity.KEY_NAME) + "! " + getBaseContext().getString(R.string.WhatIsYourLastname));
        Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText2);
                Toast.makeText(v.getContext(),getBaseContext().getString(R.string.Hello)+" "+getIntent().getStringExtra("name")+" "+editText.getText(),Toast.LENGTH_LONG).show();
                //moveTaskToBack(true);
                finish();
            }
        });
    }
}
